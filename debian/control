Source: ros-rosdistro
Section: python
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Jochen Sprickerhof <jspricke@debian.org>, Leopold Palomo-Avellaneda <leo@alaxarxa.net>
Build-Depends: debhelper-compat (= 13),
 dh-sequence-python3,
 python3-all,
 python3-setuptools,
 python3-yaml,
 python3-catkin-pkg,
 python3-rospkg,
 python3-rosdep2 <!nocheck>,
 python3-pytest <!nocheck>,
 python3-git <!nocheck>,
Standards-Version: 4.6.1
Homepage: https://wiki.ros.org/rosdistro
Vcs-Git: https://salsa.debian.org/science-team/ros-rosdistro.git
Vcs-Browser: https://salsa.debian.org/science-team/ros-rosdistro
Testsuite: autopkgtest-pkg-pybuild
Rules-Requires-Root: no

Package: python3-rosdistro
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}
Description: Tool to work with rosdistro files (for Robot OS, Python 3)
 This package is part of Robot OS (ROS). The rosdistro tool allows you
 to get access to the full dependency tree and the vcs information of
 all packages and repositories.
 .
 rosdistro is a file format for managing ROS Distributions and the ROS
 stacks they contain. This file format is used as input to a variety
 of tools in the ROS build and release toolchain, from stack release
 tools to rosdoc. The rosdistro format has changed for Catkin-based
 repositories.
 .
 This package installs the library for Python 3.
